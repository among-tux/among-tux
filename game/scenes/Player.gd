extends KinematicBody2D

export var speed = 500
export var acceleration = 7500
var velocity = Vector2.ZERO
var collision_offset

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$AnimatedSprite.self_modulate.r = rand_range(0, 1)
	$AnimatedSprite.self_modulate.g = rand_range(0, 1)
	$AnimatedSprite.self_modulate.b = rand_range(0, 1)
	velocity = Vector2.ZERO
	collision_offset = $CollisionShape2D.position.x
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var targetVelocity = Vector2.ZERO
	if Input.is_action_pressed("move_up"):
		targetVelocity.y -= 1
	if Input.is_action_pressed("move_down"):
		targetVelocity.y += 1
	if Input.is_action_pressed("move_left"):
		targetVelocity.x -= 1
	if Input.is_action_pressed("move_right"):
		targetVelocity.x += 1
	
	if targetVelocity.length() > 0:
		targetVelocity = targetVelocity.normalized() * speed
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()
	velocity = velocity.move_toward(targetVelocity, delta * acceleration)
	
	if velocity.x > 0:
		$AnimatedSprite.flip_h = true
		$CollisionShape2D.position.x = -collision_offset
	elif velocity.x < 0:
		$AnimatedSprite.flip_h = false
		$CollisionShape2D.position.x = collision_offset


func _physics_process(delta):
	move_and_slide(velocity)
