extends Node2D
export(PackedScene) var pepper_vertical
export(PackedScene) var pepper_horizontal
export(PackedScene) var pepper_diagonal1
export(PackedScene) var pepper_diagonal2

# Declare member variables here. Examples:
var start_pos = Vector2.ZERO
var end_pos = Vector2.ZERO

var cutting = false
enum CUT_DIRECTION {VERTICAL, HORIZONTAL, DIAGONAL1, DIAGONAL2}

var correct_cut_direction = CUT_DIRECTION.VERTICAL
var peppers_cut = 0

func close_to_angle(angle, target):
	var room = PI / 8
	return (target - room) <= angle and angle <= (target + room) 

func do_cut():

	var cut_vector = start_pos.direction_to(end_pos)
	var angle = cut_vector.angle()

	
	var direction = null
	var shape = null
	
	var midpoint = Vector2((start_pos.x + end_pos.x) / 2, (start_pos.y + end_pos.y) / 2)
	midpoint -= $Pepper.position
	var cuts_polygon = Geometry.is_point_in_polygon(midpoint, $Pepper/CollisionPolygon2D.polygon)
	cuts_polygon = cuts_polygon and (not Geometry.is_point_in_polygon(start_pos - $Pepper.position, $Pepper/CollisionPolygon2D.polygon))
	cuts_polygon = cuts_polygon and (not Geometry.is_point_in_polygon(end_pos - $Pepper.position, $Pepper/CollisionPolygon2D.polygon))
	
	if not cuts_polygon:
		return false

	if close_to_angle(angle, 0) or close_to_angle(angle, PI) or close_to_angle(angle, -PI):
		direction = CUT_DIRECTION.HORIZONTAL
		shape = pepper_horizontal
	elif close_to_angle(angle, PI / 4) or close_to_angle(angle, -3 * PI / 4):
		direction = CUT_DIRECTION.DIAGONAL1
		shape = pepper_diagonal1
	elif close_to_angle(angle, 3 * PI / 4) or close_to_angle(angle, -PI / 4):
		direction = CUT_DIRECTION.DIAGONAL2
		shape = pepper_diagonal2
	elif close_to_angle(angle, PI / 2) or close_to_angle(angle, -PI / 2):
		direction = CUT_DIRECTION.VERTICAL
		shape = pepper_vertical
	else:
		return false
	
	if direction == correct_cut_direction:
		var shape_instance = shape.instance()
		shape_instance.position = $Pepper.position
		var extra_force = rand_range(1, 5)
		shape_instance.get_node("Piece1").linear_velocity *= extra_force
		shape_instance.get_node("Piece2").linear_velocity *= extra_force
		add_child(shape_instance)
		return true
	return false

func set_cut_direction():
	correct_cut_direction = randi() % CUT_DIRECTION.size()

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	set_cut_direction()
	#pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event is InputEventMouseButton:

		end_pos = event.position
		if not cutting:
			start_pos = event.position
		if cutting and not event.is_pressed():
			if do_cut():
				correct_cut_direction = -1
				peppers_cut += 1
				print(peppers_cut, ' peppers have been cut')
				$PepperRespawnTimer.start()
				$Pepper.visible = false
		cutting = event.is_pressed()
		
	elif event is InputEventMouseMotion:
		if cutting:
			end_pos = event.position
	update()
			
func draw_dotted_line(start_pos, end_pos):
	
	var dots = 25
	var weight_step = 1.0/dots
	for i in range(1, dots):
		var draw = i % 2 == 1
		if draw:
			var w = weight_step * i
			var draw_start = lerp(start_pos, end_pos, w - weight_step)
			var draw_end = lerp(start_pos, end_pos, w)
			draw_line(draw_start, draw_end, Color.gray, 5)
			#print(i, ' ', w - weight_step, ' ', w, ' ', draw_start, draw_end)
	

func _draw():
	if cutting:
		draw_line(start_pos, end_pos, Color.red, 10)

	var start = null
	var end = null
	match correct_cut_direction:
		CUT_DIRECTION.VERTICAL:
			start = Vector2($Pepper.position.x, $Pepper.position.y + 150)
			end = Vector2($Pepper.position.x, $Pepper.position.y - 175)
			draw_dotted_line(start, end)
		CUT_DIRECTION.HORIZONTAL:
			start = Vector2($Pepper.position.x - 125, $Pepper.position.y)
			end = Vector2($Pepper.position.x + 150, $Pepper.position.y)
			draw_dotted_line(start, end)
		CUT_DIRECTION.DIAGONAL1:
			start = Vector2($Pepper.position.x - 100, $Pepper.position.y - 100)
			end = Vector2($Pepper.position.x + 125, $Pepper.position.y + 125)
			draw_dotted_line(start, end)
		CUT_DIRECTION.DIAGONAL2:
			start = Vector2($Pepper.position.x - 100, $Pepper.position.y + 100)
			end = Vector2($Pepper.position.x + 125, $Pepper.position.y - 125)
			draw_dotted_line(start, end)
		


func _on_PepperRespawnTimer_timeout():
	set_cut_direction()
	$Pepper.visible = true
	update()
	
