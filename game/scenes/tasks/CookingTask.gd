extends Node2D


# Declare member variables here. Examples:
var start_pos = Vector2.ZERO
var end_pos = Vector2.ZERO

var cutting = false
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event is InputEventMouseButton:
		cutting = event.is_pressed()
		start_pos = event.position
		end_pos = event.position
	elif event is InputEventMouseMotion:
		if cutting:
			end_pos = event.position
	update()
			

func _draw():
	
	if cutting:
		draw_line(start_pos, end_pos, Color.red, 10)
