# Among Tux

## License
AGPLv3

## Music license
"Club Seamus" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 4.0 License
http://creativecommons.org/licenses/by/4.0/
